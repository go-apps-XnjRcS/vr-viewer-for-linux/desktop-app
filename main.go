package main

import (
  "app/models"
  "github.com/fatih/color"
  "github.com/leaanthony/mewn"
  "github.com/wailsapp/wails"
  "os"
  "os/exec"
)

func Init() *models.MainStruct {
  MS := &models.MainStruct{}
  return MS
}

func main() {

  //check is ffmpeg installed
  if !models.Helpers_isCommandExists("ffmpeg") {
    color.Red("ffmpeg - is not installed. Please install it before...")

    comm := "sudo apt update -y && sudo apt install ffmpeg -y"
    cmd := exec.Command("sh", "-c", comm)
    cmd.Stdout = os.Stdout
    cmd.Stdin = os.Stdin
    cmd.Run()
  }

  js := mewn.String("./frontend/dist/app.js")
  css := mewn.String("./frontend/dist/app.css")

  app := wails.CreateApp(&wails.AppConfig{
    //Width:  1024,
    //Height: 768,

    Resizable: true,
    Title:     "vr-player",
    JS:        js,
    CSS:       css,
    Colour:    "#131313",
  })
  app.Bind(Init())
  app.Run()
}
