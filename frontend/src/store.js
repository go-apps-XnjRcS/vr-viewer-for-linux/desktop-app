import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    MS: null,
    can_select: false,
    can_open_dir: true,
    static_files_url: "",
  },
  getters: {
    
  },
  mutations: {
    set(state, data) {
      state[data[0]] = data[1]
    }
  },
  actions: {

  }
})
