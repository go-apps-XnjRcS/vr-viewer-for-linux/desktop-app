import Vue from 'vue'

import './styles/quasar.styl'
import '@quasar/extras/roboto-font/roboto-font.css'
import '@quasar/extras/material-icons/material-icons.css'

import {
  QAvatar,
  QBanner,
  QBtn,
  QBreadcrumbs,
  QBreadcrumbsEl,
  QDrawer,
  QFooter,
  QHeader,
  QIcon,
  QItem,
  QItemLabel,
  QItemSection,
  QLayout,
  QList,
  QPage,
  QPageContainer,
  QLinearProgress,
  QRouteTab,
  QTab,
  Notify,
  QTooltip,
  QTabs,
  QToolbar,
  QToolbarTitle,
  QSpace,
  QSlider,
  Quasar,
} from 'quasar'

Vue.use(Quasar, {
  config: {},
  components: {
    QLayout,
    QAvatar,
    QBanner,
    QBreadcrumbs,
    QBreadcrumbsEl,
    QFooter,
    QHeader,
    QDrawer,
    QPageContainer,
    QPage,
    QLinearProgress,
    QToolbar,
    QToolbarTitle,
    QBtn,
    QIcon,
    QList,
    QItem,
    QItemSection,
    QItemLabel,
    QTabs,
    QTooltip,
    QTab,
    QRouteTab,
    QSpace,
    QSlider,
  },
  directives: {},
  plugins: {
    Notify,

  },
  animations: 'all'
})