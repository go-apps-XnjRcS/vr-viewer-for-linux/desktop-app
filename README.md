## VR-PLAYER

> **FROZEN**
>
> Reason: https://github.com/wailsapp/wails/issues/237#issuecomment-531596538 
> Wails.app use webview which not supported WebGL.. 😞. 

I love VR (360 degrees) photography, but after a long search I have not found a normal software for linux, to view vr photos and videos. And if that's the case, then it's time to fix it : )
If you're like me, and want to fully use your linux, not to use crutches the type of Wine, contribute, or support the development by donation. 

### Stack of technology:
- GO 1.13
- [Wails.app](https://wails.app) as a backend framework
- [Quasar](https://quasar.dev) as frontend framework


### What is ready:
in webview

![](./app_assets/in-webview.mp4)


in browser

![](./app_assets/in-browser.mp4)


### Support us
Donate with skrill.com. e-mail **sht_job@ukr.net**, id - **22550990**

Thanks 🤗

