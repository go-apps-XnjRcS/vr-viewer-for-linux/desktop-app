module app

require (
	github.com/disintegration/imaging v1.6.1
	github.com/fatih/color v1.7.0
	github.com/leaanthony/mewn v0.10.7
	github.com/mitchellh/go-homedir v1.1.0
	github.com/pkg/errors v0.8.1
	github.com/wailsapp/wails v0.17.0
	github.com/yale8848/gffmpeg v0.0.0-20190226031624-d9a1ca215ba8
)

go 1.13
