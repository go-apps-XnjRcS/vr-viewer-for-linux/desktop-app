package models

import (
  "errors"
  "fmt"
  "github.com/disintegration/imaging"
  "io/ioutil"
  "os"
  "os/exec"
  "path/filepath"
  "strings"
  "sync"
)


type openDirStruct struct {
	path string
	total int
	files []os.FileInfo
}
type savedFileStruct struct {
  FileType string `json:"file_type"`
  Filename string `json:"filename"`
  OrigPath string `json:"orig_path"`
  Error    string  `json:"error"`
}

type projectPathsStruct struct {
  home,  static,  static_big,  static_small,  settings string
}


func (O *MainStruct) readDir(path string) (openDirStruct, error) {

  var dirInfo openDirStruct

  //для начала откроем этот путь
  pathInfo, err := os.Stat(path)
  if err != nil{
    return dirInfo, err
  }

  if !pathInfo.IsDir(){
    err = errors.New("path is not a directory")
    return dirInfo, err
  }

  files, err := ioutil.ReadDir(path)
  if err != nil {
    return dirInfo, err
  }

  if len(files) == 0 {
    return dirInfo, errors.New("selected path is empty")
  }

  var n int
  for _, v := range files{
    ext := strings.ToLower(filepath.Ext(v.Name()))
    ext = strings.TrimPrefix(ext, ".")
    if ext == "jpg" || ext == "jpeg" || ext == "png" || ext == "mp4"{
      files[n] = v
      n++
    }
  }
  files = files[:n]

  dirInfo.total = len(files)
  dirInfo.path = path
  dirInfo.files = files

  if len(files) == 0 {
    return dirInfo, errors.New("no supported media files in selected path")
  }


  return dirInfo, nil
}

func (O *MainStruct) saveFiles(DS openDirStruct) []savedFileStruct {
  if DS.total == 0 {
    return nil
  }

  var wg sync.WaitGroup
  var res []savedFileStruct
  wg.Add(DS.total)

  for _, f := range DS.files{
    ext := strings.Trim(filepath.Ext(f.Name()),".")
    if ext != "mp4"{
      go savePhoto(f, DS.path, &res, &wg, O)
    }else{
      go saveVideo(f, DS.path, &res, &wg, O)
    }
    //time.Sleep(time.Second)
  }

  wg.Wait()
  return res
}

func savePhoto(
  f         os.FileInfo,
  origPath  string,
  res       *[]savedFileStruct,
  wg        *sync.WaitGroup,
  O         *MainStruct,
  ) {

  defer wg.Done()

  var tmp savedFileStruct
  ext := filepath.Ext(f.Name())
  newFileName := fmt.Sprintf(
    "%s-%d%s",
    f.Name()[0: len(f.Name()) - len(ext)],
    f.Size(),
    ext,
    )

  newPath := O.projectPaths.static_small + newFileName
  if err := Helpers_dirPrepare(filepath.Dir(newPath)); err != nil{
    tmp.Error = err.Error()
    *res = append(*res, tmp)
    O.runtime.Events.Emit("ReadFile", tmp)
    return
  }

  tmp.FileType = "photo"
  tmp.Error = ""
  tmp.Filename = newFileName
  tmp.OrigPath = fmt.Sprintf("%s/%s", origPath, f.Name())

  //big thumb (symlink on original file)
  if err := saveLinkOnOrigFile(tmp.OrigPath, newFileName, O); err !=nil{
    tmp.Error = err.Error()
    *res = append(*res, tmp)
    O.runtime.Events.Emit("ReadFile", tmp)
    return
  }

  //small thumb
  if Helpers_fileExists(newPath){
    *res = append(*res, tmp)
    O.runtime.Events.Emit("ReadFile", tmp)
    return
  }

  //create thumbnail for image
  img, err := imaging.Open(tmp.OrigPath, imaging.AutoOrientation(true))
  if err != nil {
    tmp.Error = err.Error()
    *res = append(*res, tmp)
    O.runtime.Events.Emit("ReadFile", tmp)
    return
  }


  img = imaging.Resize(img, 450, 0, imaging.NearestNeighbor)
  if err := imaging.Save(img, newPath, imaging.JPEGQuality(45)); err !=nil{
    tmp.Error = err.Error()
    *res = append(*res, tmp)
    O.runtime.Events.Emit("ReadFile", tmp)
    return
  }

  *res = append(*res, tmp)
  O.runtime.Events.Emit("ReadFile", tmp)
}

func saveVideo(
  f os.FileInfo,
  origPath string,
  res *[]savedFileStruct,
  wg *sync.WaitGroup,
  O *MainStruct,
  ) {
  defer wg.Done()

  var tmp savedFileStruct
  ext := filepath.Ext(f.Name())
  newFileName := fmt.Sprintf(
    "%s-%d",
    f.Name()[0: len(f.Name()) - len(ext)],
    f.Size(),
    )

  newPath := O.projectPaths.static_small + newFileName + ".jpg"
  if err := Helpers_dirPrepare(filepath.Dir(newPath)); err != nil{
    tmp.Error = err.Error()
    *res = append(*res, tmp)
    O.runtime.Events.Emit("ReadFile", tmp)
    return
  }

  tmp.FileType = "video"
  tmp.Error = ""
  tmp.Filename = newFileName //need to remember that is name without extension
  tmp.OrigPath = fmt.Sprintf("%s/%s", origPath, f.Name())

  //big thumb (symlink on original file)
  if err := saveLinkOnOrigFile(tmp.OrigPath, newFileName+ext, O); err !=nil{
    tmp.Error = err.Error()
    *res = append(*res, tmp)
    O.runtime.Events.Emit("ReadFile", tmp)
    return
  }

  //small thumb check
  if Helpers_fileExists(newPath){
    *res = append(*res, tmp)
    O.runtime.Events.Emit("ReadFile", tmp)
    return
  }

  //create thumbnail for image
  if err := saveVideoThumb(tmp.OrigPath, newPath) ;err != nil {
    tmp.Error = err.Error()
    *res = append(*res, tmp)
    O.runtime.Events.Emit("ReadFile", tmp)
    return
  }

  *res = append(*res, tmp)
  O.runtime.Events.Emit("ReadFile", tmp)
}

func saveLinkOnOrigFile(origPath string, newFilename string, O *MainStruct) error {
  newPath := O.projectPaths.static_big+newFilename

  if err := Helpers_dirPrepare(filepath.Dir(newPath)); err != nil{
    return err
  }

  //check is link already exists?
  if !Helpers_fileExists(newPath){
    if err := os.Symlink(origPath, newPath); err != nil{
      return err
    }
  }

  return nil
}
func saveVideoThumb(inputVideoPath string, outImgPath string) error {
  cmand := fmt.Sprintf("ffmpeg -i %s -s 450x225 -vframes 1 %s -y", inputVideoPath, outImgPath)
  err := exec.Command("sh", "-c", cmand).Run()
  return err
}

