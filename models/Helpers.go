package models

import (
	"github.com/fatih/color"
	"os"
	"os/exec"
	"time"
)

func Helpers_fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func Helpers_isCommandExists(name string) bool {
	err := exec.Command("/bin/sh", "-c", "command -v "+name).Run()
	if err != nil {
		color.Red(err.Error())
		time.Sleep(time.Second * 2)
		return false
	}
	return true
}

func Helpers_dirPrepare(dirPath string) error {
	_, err := os.Stat(dirPath)
	if !os.IsNotExist(err) {
		return nil
	}

	return os.MkdirAll(dirPath, 0744)
}