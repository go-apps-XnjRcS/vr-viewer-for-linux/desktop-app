package models

import (
  "fmt"
  "log"
  "net/http"
)

func handlerForServer(w http.ResponseWriter, req *http.Request) {
  w.Header().Set("Access-Control-Allow-Origin", "*")
  w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
  fmt.Fprint(w)
}


func (O *MainStruct) initServer(pathToStatic string, c chan <- string ){

  port := "46424"
  //http.HandleFunc("/", handlerForServer)
  //http.Handle("/static/", http.StripPrefix(strings.TrimRight("/static/", "/"), http.FileServer(http.Dir(pathToStatic))))

  var orig = http.StripPrefix("/static/", http.FileServer(http.Dir(pathToStatic)))
  var wrapped = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Access-Control-Allow-Origin", "*")
    // …

    orig.ServeHTTP(w, r)
  })

  http.Handle("/static/", wrapped)


  log.Printf("Serving %s on HTTP port: %s\n", pathToStatic, port)
  c <- fmt.Sprintf("http://localhost:%s/static/", port)
  log.Fatal(http.ListenAndServe(":"+port, nil))



}
