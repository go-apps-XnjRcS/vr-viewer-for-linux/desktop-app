package models

import (
	"fmt"
	"github.com/mitchellh/go-homedir"
	"github.com/wailsapp/wails"
	"os"
)

type MainStruct struct {
	urlForMedia string
	runtime *wails.Runtime
	log *wails.CustomLogger
	projectPaths projectPathsStruct
}

func (O *MainStruct) WailsInit(r *wails.Runtime) error {
	O.runtime = r
	O.log = r.Log.New("MainStruct")

	//initialize directories for settings and files
	O.initAppDir()

	serverChanel := make(chan string, 1)
	go O.initServer(O.projectPaths.static, serverChanel)
	O.urlForMedia = <- serverChanel

	return nil
}

func (O *MainStruct) GetUrlForStatic() string {
	return O.urlForMedia
}

func (O *MainStruct) OpenDir() (string, error) {
	var res string
	selectedPath := O.runtime.Dialog.SelectDirectory()
	//todo для теста подставим путь. после убрать
	//selectedPath := "/home/shturnev/Изображения/Antalya2019"

	if selectedPath == "" {
		return res, nil
	}

	O.runtime.Events.Emit("OpenDirPath", selectedPath)
	O.log.Info(selectedPath)
	res = selectedPath

	//At first we need to read a selected dir
	readRes, err := O.readDir(selectedPath)
	if err != nil{
		return res, err
	}

	//Next. read files from opened dir
	O.runtime.Events.Emit("StartReadFiles", readRes.total)
	resSaved := O.saveFiles(readRes)

	//At last, send a notice that we finish
	O.runtime.Events.Emit("FinishReadFiles", resSaved)


	fmt.Println(resSaved)

	return res, nil
}

func (O *MainStruct) initAppDir() map[string]string {

	hDir, err := homedir.Dir()
	if err != nil {
		O.log.Fatalf("%s", err)
	}

	hDir += "/.sht-vr-player"

	paths := map[string]string{
		"home":  	hDir,
		"static": 	hDir + "/static/",
		"static_big": 	hDir + "/static/big/",
		"static_small": 	hDir + "/static/small/",
		"settings": hDir + "/settings/",
	}


	for _, path :=range paths {
		if _, err := os.Stat(path); os.IsNotExist(err) {
			if err := os.Mkdir(path, 0744); err != nil{
				O.log.Errorf("%s", err)
			}
		}
	}

	O.projectPaths.home 				= paths["home"]
	O.projectPaths.settings 		= paths["settings"]
	O.projectPaths.static 			= paths["static"]
	O.projectPaths.static_big 	= paths["static_big"]
	O.projectPaths.static_small = paths["static_small"]

	return paths
}



/*func (O *MainStruct) createSymLink(paths map[string]string){
	//todo это всё хорошо, но всёравно нужно будет создавать превьюшки, иначе комп не вытянет по ресурсам
	oldPath := "/home/shturnev/Изображения/Antalya2019/IMG_20190912_200557_00_009.jpg"
	ext := filepath.Ext(oldPath)

	f, err := os.Stat(oldPath)
	if err !=nil {
		O.log.Fatal(err.Error())
		return
	}

	s := fmt.Sprintf("%d-%s", f.Size(), f.Name())
	newName := fmt.Sprintf("%x%s", md5.Sum([]byte(s)), ext)
	newPath := fmt.Sprintf("%s/%s", paths["static"], newName)

	if Helpers_fileExists(newPath){
		O.log.Infof("%s is already exists", newName)
		return
	}

	err = os.Symlink(oldPath, newPath)
	if err !=nil {
		O.log.Fatal(err.Error())
		return
	}
}*/


